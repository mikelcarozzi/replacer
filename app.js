(function () {
  "use strict";

  const path = require('path')
  const fs = require('fs')
  const walk = require('walk');
  const _ = require('lodash')

  let options = {
    followLinks: false
    , filters: ["Temp", "_Temp"]
  };

  let walker, _path;
  let _report = []
  let _dotnotation = []
  let _positions = []
  let _filesModified = []
  let _matchesGroup = {}
  let _idioma = JSON.parse(fs.readFileSync('../traducciones/idioma.json'))

  walker = walk.walk("../itms-front-end/src/app/modules", options);

  walker.on("names", function (root, nodeNamesArray) {
    _path = root + '/'
  });

  walker.on("file", function (root, fileStats, next) {
    fs.readFile(fileStats.name, function () {

      let ext = path.extname(fileStats.name)

      let filter = [
        'file',
        '.html'
      ]

      if (filter.indexOf(fileStats.type) >= 0 && filter.indexOf(ext) >= 0) {
        let _file = _path + fileStats.name
        readFile(_file)
      }

      next();
    });
  });

  walker.on("errors", function (root, nodeStatsArray, next) {
    next();
  });

  walker.on("end", function () {
    _deepSearch(_idioma, _dotnotation)
    _replace()
  });

  /*
    lee el archivo
  */

  function readFile(file) {
    let _file = fs.readFileSync(file)
    match(_file.toString(), file)
  }

  /*
    match con expresion regular
  */

  function match(_str, _file) {

    _str = _str.replace(/\n|\t/gms, '')
    _str = _str.replace(/Nº/g, 'N° ')

    // TODO: no detecta los <i> hijos de un <div>
    let _matches = _str.match(/(?<=\>)(.[^/>]+)(<\/[^</div]+)/gs)

    if (_matches) {
      let _inner = _matches.map((_e) => {
        let _type = _e.match(/(?<=\<\/)(.*?)(?=\>)/gs) // obtiene el tag
        let _tag = _e.match(/(\<)(.*?)(\>)/gs) //obtiene el contexto del string
        let _plain = _e.replace(/<[^>]*>?/gs, ''); // obtiene el texto plano dentro de los tags html

        if (_type) {
          report(_file, _e, _type[0].trim(), _plain.trim())
        }
      })
    }
  }


  /**
   * construir reporte
   */

  function report(_file, _match, _tag, _plain) {
    if (_plain != '') {
      _report.push({
        file: _file,
        match: _match.replace('/\s\s+/g', '').replace(/\n/g, ' ').trim(),
        tag: _tag,
        plain: _plain.trim()
      })
    }
  }

  /**
   * _deepSearch
   */

  function _deepSearch(_object, _array) {
    let _next = true

    for (const key in _object) {
      if (_object.hasOwnProperty(key)) {

        const element = _object[key];

        // let _type = 'label'

        // if (_array.indexOf('table') >= 0) _type = 'table'
        // if (key.match(/btn/g)) _type = 'button'
        // if (element.hasOwnProperty('title')) _type = 'title'

        // _positions.push({
        //   key: key,
        //   text: element.label.toLowerCase(),
        //   type: _type,
        //   position: [..._array, key]
        // })

        if (element.hasOwnProperty('label')) {

          let _type = 'label'

          if (_array.indexOf('table') >= 0) _type = 'table'
          if (key.match(/btn/g)) _type = 'button'
          if (element.hasOwnProperty('title')) _type = 'title'

          _positions.push({
            key: key,
            text: element.label,
            type: _type,
            position: [..._array, key]
          })
        }

        if (element.hasOwnProperty('title')) {
          _positions.push({
            key: key,
            text: element.title,
            type: 'title',
            position: [..._array, key]
          })
        }

        if (typeof element === 'object') {
          _deepSearch(element, [..._array, key])
        }
      }
    }
  }

  /**
   * _replace
     * _titles -> _report item
     * { file:
          '../itms-front-end/src/app/modules/profesionales/pacientes/pacientes.component.html',
          match: 'Mis pacientes</h2>    ',
          tag: 'h2',
          plain: 'mis pacientes' 
        }
   * _positions elementos del archivo de idioma *
   *   { key: 'allergy',
        text: 'alergias',
        type: 'label',
        position: [ 'dashboard', 'home', 'myHealth', 'modal', 'allergy' ] 
      }
      
   * match de objetos
   * MATCH UN TITLE
   * 
      item de _positions

     { key: 'nextService',
       text: 'próxima consulta',
       type: 'title',
       position: [ 'dashboard', 'home', 'tab', 'home', 'nextService' ] }

       item de _report

     { file:
       '../itms-front-end/src/app/modules/profesionales/inicio/inicio.component.html',
       match: 'Próxima Consulta</h5>        ',
       tag: 'h5',
       plain: 'próxima consulta' 
     }

 */

  async function _replace() {
    let _ths = []
    let _buttons = []
    let _titles = []
    let _labels = []
    let _type, _htmlType

    let _htmlTitles = ['h1', 'h2', 'h3', 'h4', 'h5']
    let _htmlLabels = ['p', 'span', 'label', 'b', 'ul', 'strong', 'small', 'i']
    let _htmlTables = ['th']
    let _htmlButtons = ['a', 'button']
    let _exclude = ['ng-template']

    for (let i = 0; i < _report.length; i++) {
      const element = _report[i];

      if (_htmlTitles.indexOf(element.tag) >= 0) {
        _titles.push(element)
        delete _report[i]
      }

      if (_htmlLabels.indexOf(element.tag) >= 0) {
        _labels.push(element)
        delete _report[i]
      }

      if (_htmlButtons.indexOf(element.tag) >= 0) {
        _buttons.push(element)
        delete _report[i]
      }

      if (_htmlTables.indexOf(element.tag) >= 0) {
        _ths.push(element)
        delete _report[i]
      }

      if (_exclude.indexOf(element.tag) >= 0) {
        delete _report[i]
      }
    }

    // se agrupan los elementos de _report
    _matchesGroup = {
      title: _titles,
      label: _labels,
      button: _buttons,
      table: _ths
    }

    for (const key in _matchesGroup) {
      if (_matchesGroup.hasOwnProperty(key)) {
        const element = _matchesGroup[key];

        for (let i = 0; i < element.length; i++) {
          const matchItem = element[i];
          /**
           * { file:
              '../itms-front-end/src/app/modules/admin/modules/admin-profiles/components/crear-perfil/crear-perfil.component.html',
              match: 'Estado</th>                    ',
              tag: 'th',
              plain: 'estado' 
            }
           */

          for (let j = 0; j < _positions.length; j++) {
            const langItem = _positions[j];

            // { key: 'nextService',
            //    text: 'próxima consulta',
            //    type: 'title', label, button, table
            //    position: [ 'dashboard', 'home', 'tab', 'home', 'nextService' ] }

            if (matchItem.plain == langItem.text) {
              _htmlType = langItem.type == 'title' ? '.title' : '.label'
              let _replaceStrNg = "{{ '" + langItem.position.join('.') + _htmlType + "' | transloco }}"

              // DEBUG
              if (matchItem.file == '../itms-front-end/src/app/modules/pacientes/modules/agendar/components/index/index.component.html') {
                // await _writeFile(matchItem.file, matchItem.plain, _replaceStrNg)
              }

              await _writeFile(matchItem.file, matchItem.plain, _replaceStrNg)
            }

          }
        }
      }
    }

    _filesModified.push({
      total: _filesModified.length
    })

    _htmlReport()
  }

  /**
   * _writeFile
   */

  async function _writeFile(_file, _needle, _replace) {
    let _fileStr = fs.readFileSync(_file).toString()
    let _regex = new RegExp('(?<!#|"|"sin| sin|permiso|AAAUIAAA|7Y8lQL|ter|rHQ6Aog|QjyOpTIu|Nib05yRm|3dVWZDg|3mtWpN|l5GIc|mqlUW8|WJWFL|FazFxb1|QWtr|YwjZE2|ePAd6W|f0JDo|7251nx|\\.)(' + _needle + ')(?!\\(| =|=|")', 'gs')

    let _occour = _fileStr.search(_needle)

    if (_occour >= 0) {
      let _strMatcheshtml = _fileStr.match(_regex)
      _fileStr = _fileStr.replace(_regex, _replace)

      fs.writeFileSync(_file, _fileStr)

      _strMatcheshtml.map((_e) => {
        _filesModified.push({
          file: _file,
          needle: _needle,
          replace: _replace
        })
      })

    } else {
      console.log(_needle)
    }

    // if (_regex.test(_fileStr)) {
    //   let _strMatcheshtml = _fileStr.match(_regex)

    //   _fileStr = _fileStr.replace(_regex, _replace)
    //   fs.writeFileSync(_file, _fileStr)

    //   _strMatcheshtml.map((_e) => {
    //     _filesModified.push({
    //       file: _file,
    //       needle: _needle,
    //       replace: _replace
    //     })
    //   })
    // }
  }

  /**
   * _htmlReport
   */

  function _htmlReport() {
    let _strO = '<html><body><head><style>.container{width: 100%; margin: 0 auto;} .row {line-height: 24pt; border: solid 1px black; padding-left: 10px} div.container>div:nth-of-type(odd) {background: #e0e0e0;}</style></head><div class="container">'
    let _strC = '</body></html>'

    _strO += '<h1>Reporte Reemplazos</h1>'
    _strO += '<h4>Fecha Creación: ' + new Date() + '</h4>'
    _strO += '<h4> Total Reemplazos: ' + (_filesModified.length - 1) + '</h4>'

    _filesModified.pop()

    _filesModified.map((_e) => {
      _strO += '<div class="row">'
      _strO += '<span> Archivo: <i>' + _e.file + '</i></span><br>'
      _strO += '<span> Needle: <i>' + _e.needle + '</i></span><br>'
      _strO += '<span> Reemplazo: <i>' + _e.replace + '</i></span><br>'
      _strO += '</div>'
      _strO += '<br>'
    })

    fs.writeFileSync(__dirname + '/report.html', _strO + _strC)
  }

}());